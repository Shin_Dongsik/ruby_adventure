﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCController : MonoBehaviour
{
    public float dialogDisplayTime = 4.0f;
    public GameObject dialogBox;

    float displayTimer;

    // Start is called before the first frame update
    void Start()
    {
        dialogBox.SetActive(false);
        displayTimer = -1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if(displayTimer >= 0)
        {
            displayTimer -= Time.deltaTime;

            if (displayTimer <= 0)
            {
                dialogBox.SetActive(false);
            }
        }
    }

    public void DisplayDialog()
    {
        displayTimer = dialogDisplayTime;
        dialogBox.SetActive(true);
    }
}
