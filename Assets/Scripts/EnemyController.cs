﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float speed = 2.0f;
    public bool vertical;
    public float changeTime = 3.0f;
    public ParticleSystem smokeEffect;
    public AudioClip hitPlayerClip;

    Rigidbody2D rigidbody2D;
    float timer;
    int direction = 1;
    Animator animator;
    bool isBroken;
    AudioSource audioSource;
    
    // Start is called before the first frame update
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        timer = changeTime;
        animator = GetComponent<Animator>();
        isBroken = false;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isBroken)
            return;

        timer -= Time.deltaTime;

        if(timer < 0)
        {
            direction = -direction;
            timer = changeTime;
        }

        Vector2 position = rigidbody2D.position;

        if (vertical)
        {
            animator.SetFloat("MoveX", 0);
            animator.SetFloat("MoveY", direction);
            position.y = position.y + speed * Time.deltaTime * direction;
        }
        else
        {
            animator.SetFloat("MoveX", direction);
            animator.SetFloat("MoveY", 0);
            position.x = position.x + speed * Time.deltaTime * direction;
        }
        
        rigidbody2D.MovePosition(position);
    }

    //무엇인가와 충돌하면
    private void OnCollisionEnter2D(Collision2D collision)
    {
        RubyController player = collision.gameObject.GetComponent<RubyController>();

        if(player != null)
        {
            player.changeHealth(-1);
            player.PlaySound(hitPlayerClip);
        }
    }

    public void FixRobot()
    {
        isBroken = true;
        rigidbody2D.simulated = false;

        animator.SetTrigger("Fixed");
        smokeEffect.Stop();
        audioSource.PlayOneShot(audioSource.clip);
    }
}
