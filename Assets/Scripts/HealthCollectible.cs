﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthCollectible : MonoBehaviour
{

    public AudioClip collectedClip;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        RubyController controller = collision.GetComponent<RubyController>();

        if(controller != null)
        {
            if(controller.Health < controller.maxHealth)
            {
                controller.changeHealth(1);
                Destroy(gameObject);

                controller.PlaySound(collectedClip);
            }
            
        }

        //Debug.Log("Object that entered the trigger : " + collision);        
    }
}
