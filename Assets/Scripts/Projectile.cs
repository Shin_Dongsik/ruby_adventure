﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float maxTimer = 4.0f;
    public AudioClip bulletClip;

    Rigidbody2D rigidbody2D;
    float currentTimer = 0.0f;

    // Start is called before the first frame update
    void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if(currentTimer >= maxTimer)
        {
            Destroy(gameObject);
        }

        currentTimer += Time.deltaTime;
        
        /*
        if(transform.position.magnitude > 1000.0f)
        {
            Destroy(gameObject);
        }
        */
    }

    //발사
    public void Launch(Vector2 direction, float force, GameObject gameObject)
    {
        rigidbody2D.AddForce(direction * force);
        gameObject.GetComponent<RubyController>().PlaySound(bulletClip);
    }

    //충돌감지
    private void OnCollisionEnter2D(Collision2D collision)
    {
        EnemyController ec = collision.collider.GetComponent<EnemyController>();

        if(ec != null)
        {
            ec.FixRobot();
        }
        
        Destroy(gameObject);
    }
}
