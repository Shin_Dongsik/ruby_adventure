﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageZone : MonoBehaviour
{
    public AudioClip hitPlayerClip;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        RubyController player = collision.GetComponent<RubyController>();

        if(player != null)
        {
            player.changeHealth(-1);
            player.PlaySound(hitPlayerClip);
        }
    }
}
