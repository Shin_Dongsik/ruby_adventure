﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RubyController : MonoBehaviour
{
    public float speed = 3.0f;
    public int maxHealth = 5;
    public float timeInvincible = 2.0f;
    public GameObject projectilePrefab;

    int currentHealth;
    Rigidbody2D rigidbody2d;
    float invincibleTimer;
    bool isInvincible;
    Animator animator;
    Vector2 loolDirection = new Vector2(1, 0);
    AudioSource audioSource;

    public int Health { get { return currentHealth; } }

    // Start is called before the first frame update
    void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        currentHealth = maxHealth;

        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        Vector2 move = new Vector2(horizontal, vertical);

        if(!Mathf.Approximately(move.x, 0.0f) || !Mathf.Approximately(move.y, 0.0f))
        {
            loolDirection.Set(move.x, move.y);
            loolDirection.Normalize();
        }

        animator.SetFloat("MoveX", loolDirection.x);
        animator.SetFloat("MoveY", loolDirection.y);
        animator.SetFloat("Speed", move.magnitude);

        Vector2 position = rigidbody2d.position;
        position = position + move * speed * Time.deltaTime;
        rigidbody2d.MovePosition(position);

        if (isInvincible)
        {
            invincibleTimer -= Time.deltaTime;

            if (invincibleTimer < 0)
            {
                isInvincible = false;
            }
        }

        //무기발사
        if (Input.GetKeyDown(KeyCode.C))
        {
            Launch();
        }

        //상호작용
        if (Input.GetKeyDown(KeyCode.X))
        {
            RaycastHit2D hit = Physics2D.Raycast(rigidbody2d.position + Vector2.up * 0.2f, loolDirection
                , 1.5f, LayerMask.GetMask("NPC"));

            if(hit.collider != null)
            {
                hit.collider.GetComponent<NPCController>().DisplayDialog();
            }
        }
        
    }

    //체력이 변하는경우
    public void changeHealth(int amount)
    {
        if(amount < 0)
        {
            if (isInvincible)
                return;

            isInvincible = true;
            invincibleTimer = timeInvincible;
        }

        currentHealth = Mathf.Clamp(currentHealth + amount, 0, maxHealth);
        UIHealthBar.instance.SetValue(currentHealth / (float)maxHealth);
        //Debug.Log(currentHealth + "/" + maxHealth);
    }

    //무기 발사
    void Launch()
    {
        GameObject projectileObject = Instantiate(projectilePrefab, rigidbody2d.position + Vector2.up * 0.5f, Quaternion.identity);
        Projectile projectile = projectileObject.GetComponent<Projectile>();
        projectile.Launch(loolDirection, 300, gameObject);

        animator.SetTrigger("Launch");
    }

    //오디오 클립 플레이
    public void PlaySound(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }
}
